from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os
from .main import app, db


class Comic(db.Model):
    __tablename__ = 'comic'

    id = db.Column(db.Integer,primary_key = True)
    title = db.Column(db.String(150), nullable = False)
    description = db.Column(db.String(3000))
    issueNumber = db.Column(db.Integer)
    isbn = db.Column(db.String(100))
    series = db.Column(db.String(200))
    price = db.Column(db.Integer)
    wiki = db.Column(db.String(200))
    thumbnail = db.Column(db.String(200))
    characters = db.Column(db.ARRAY(db.String(10)))
    events = db.Column(db.ARRAY(db.Integer))

try:  
    db.drop_all()
    db.create_all()
except:
    db.create_all()