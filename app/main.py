from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
import os
import json

  
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/comics/')
def comics():
  from .models import Comic
  comic_list = db.session.query(Comic).all()
  return render_template('comics.html', comics=comic_list)