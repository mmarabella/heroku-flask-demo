from .main import app, db
from .models import Comic


def create_db():
    comics = [{ "id": 6293, "title": "ANNIHILATION BOOK 2 TPB (Trade Paperback)", "description": "The epic collection of the cosmic event continues, as the the Annihilation Wave targets Galactus' ex-heralds! Hunted by beings every bit as powerful as himself, the Silver Surfer must find a way to unite the one-time heralds against a common foe. But can even the combined cosmic might of those who served Galactus stand against these powerful beings!? Meanwhile, no longer recognized as \"Accuser,\" Ronan himself has been accused, tried and stripped of his rank. Now, he's out to clear his name! And finally, from one of the writers of ABC's smash hit Lost comes a new vision of a venerable villain as Super-Skrull brings his devious cunning to bear on an unstoppable enemy! Super-Skrull bursts into the frontline of the war against the Annihilation Wave, taking the fight to the enemy in his own ruthless way! Collecting SILVER SURFER #1-4, SUPER SKRULL #1-4 and RONAN #1-4.\r<br>312 PGS./Rated A ...$29.99\r<br>", "issueNumber": 2, "isbn": "978-0-7851-2902-8", "series": "ANNIHILATION BOOK 2 TPB (2011)", "price": 24.99, "wiki": "http://marvel.com/comics/collection/6292/annihilation_book_2_tpb_trade_paperback?utm_campaign=apiRef&utm_source=040305794471cc8de0bc049477956487", "thumbnail": "http://i.annihil.us/u/prod/marvel/i/mg/9/10/51ddc2048de62/portrait_fantastic.jpg", "events": [229], "characters": [] }]

    for comic in comics:
        id = comic['id']
        title = comic['title']
        description = comic['description']
        issueNumber = comic['issueNumber']
        isbn = comic['isbn']
        series = comic['series']
        price = comic['price']
        wiki = comic['wiki']
        thumbnail = comic['thumbnail']
        characters = comic['characters']
        events = comic['events']

        comicObject = Comic(id = id, title = title, description = description, issueNumber = issueNumber, isbn = isbn, series = series, price = price, wiki = wiki, thumbnail = thumbnail, characters = characters, events = events)
        db.session.add(comicObject)
        db.session.commit()


comic_list = db.session.query(Comic).all()
if len(comic_list) == 0:
    create_db()
