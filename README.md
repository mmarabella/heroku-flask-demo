# Deploy your Flask app on Heroku

To deploy your Flask app, clone this repository and follow these steps. To deploy your own custom app, modify `main.py`, `models.py`, the templates, etc.
```
heroku login
heroku create heroku-flask-demo-cs331e
git push heroku master
heroku addons:create heroku-postgresql:hobby-dev --app heroku-flask-demo-cs331e
git push heroku master
heroku open
```

